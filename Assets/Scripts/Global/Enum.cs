﻿public class Enum
{
    // General
    public enum CharacterState { Idle, Walk, Jump, Interacting }
    public enum ObjectState { Enabled, Disabled }
    public enum GroundType { Dirt, Stone, Grass }
    public enum ActivationType {OnEnable, OnTriggerEnter, OnTriggerExit };

    // Gameplay
    public enum InstrumentType { Setar, Kamanche, Daf, Guitar, Violin, Tombak, Piano }
}