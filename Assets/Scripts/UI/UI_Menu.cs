﻿using UnityEngine;

public class UI_Menu : UI_Panel
{
    [SerializeField] AudioSource musicIntro;
    [SerializeField] GameObject animIntro;
    [SerializeField] UI_Panel cinematicBars;

    private GameInput gi;

    protected override void Awake() {
        base.Awake();
        gi = GameManager.Instance.GetTool<GameInput>("GameInput");
        Show();
        gi.SetCursorVisibility(true);
        GameManager.Instance.GetTool<AudioManager>("AudioManager").Fade(musicIntro, 1f, 5f);
        musicIntro.Play();
    }

    public void OnPlay() {
        Hide();
        gi.SetCursorVisibility(false);
        cinematicBars.Show();
        animIntro.SetActive(true);
    }
}
