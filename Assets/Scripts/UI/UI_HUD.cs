using UnityEngine;
using UnityEngine.UI;

public class UI_HUD : UI_Panel
{
    [SerializeField] Sprite[] instruments;
    [SerializeField] Image instrumentImage;
    [SerializeField] GameObject holdMouseIndicator;
    [SerializeField] Text birdCounter;

    [SerializeField] UI_RequiredInstruments requiredInstrumentPanel;

    private void Start() {
        birdCounter.text = "3 / 30";
        requiredInstrumentPanel.Hide();
        Show();
    }

    public void SetCurrentInstrumentImage(Enum.InstrumentType current) {
        instrumentImage.sprite = instruments[(int)current];
    }

    public void ShowRequiredInstruments(Vector3 position, Enum.InstrumentType [] requiredInstruments, float [] melodyValues) {
        requiredInstrumentPanel.Show(position, requiredInstruments, melodyValues);
    }

    public void HideRequiredInstruments() {
        requiredInstrumentPanel.Hide();
    }

    public void ShowHoldMouseIndicator() {
        holdMouseIndicator.SetActive(true);
    }

    public void HideHoldMouseIndicator() {
        holdMouseIndicator.SetActive(false);
    }

    public void OnBirdFound(int num) {
        birdCounter.text = num + " / 30";
    }
}
