using System.Collections;
using UnityEngine;
using System;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] UI_HUD HUD;
    [SerializeField] float HUD_WorldSpace_Height;
    [SerializeField] float HUD_WorldSpace_DistMultiplier;

    [SerializeField] float speedMovement;
    [SerializeField] float speedRotation;
    [SerializeField] float jumpSpeed;
    [SerializeField] AnimationCurve jumpCurve;
    [SerializeField] float gravityMultiplier;
    [SerializeField] LayerMask walkableLayer;
    [SerializeField] float walkableRaycastDist;

    [SerializeField] Camera cam;
    [SerializeField] LayerMask interactLayer;
    [SerializeField] float interactRaycastDist;

    [SerializeField] bool useOutline = true;

    [SerializeField] InstrumentController ic;
    [SerializeField] DialogueController dc;

    [SerializeField] Material transparentMaterial;

    [SerializeField] AudioSource footsteps;
    [SerializeField] AudioClip[] footstepVariationsGrass;
    [SerializeField] AudioClip[] footstepVariationsStone;
    [SerializeField] AudioSource jump;
    [SerializeField] AudioClip[] jumpVariations;

    [SerializeField] AudioSource[] instrumentsPlay;
    [SerializeField] AudioSource[] instrumentsFail;

    [SerializeField] AudioClip[] melody3Seconds;
    [SerializeField] AudioClip[] melody5Seconds;
    [SerializeField] AudioClip[] melody7Seconds;

    [SerializeField] AudioSource birdFoundSFX;
    [SerializeField] AudioSource birdFlyingSFX;

    [SerializeField] Bird hoopoe;
    [SerializeField] Dialogue HoopoeFinalDialogue;

    [SerializeField] ObjectStateController simorghSequenceStateController;

    public bool InDialogue { get; private set; }
    public Enum.InstrumentType CurrentInstrument { get { return ic.Current; } }
    public Material TransparentMaterial { get { return transparentMaterial; } }

    private GameInput gi;
    private CharacterController cc;
    private Transform neck;
    private Vector3 motion;
    private float sensitivity = 100f;
    private float rotationX;
    private float jumpAmount;
    private Coroutine jumpRoutine;

    private int birdsFound = 3;

    private RaycastHit hitGround;
    private RaycastHit hit;
    private Collider interactedCollider;
    private Interact interactedTmp;
    private Interact interactedCurrect;
    private Interact interactedPrevious;

    private Enum.InstrumentType previousInstrument;
    private Coroutine completeMelodyRoutine;

    private Action dialogueCallback;
    private bool locked = false;

    private void Awake() {
        gi = GameManager.Instance.GetTool<GameInput>("GameInput");
        cc = GetComponent<CharacterController>();
        neck = cam.transform.parent;
        ic.Show();
    }

    private void Update() {
        if (locked) return;
        if (InDialogue == false) {
            UpdateInstruments();
            HUD.SetCurrentInstrumentImage(ic.Current);
            UpdateMovement();
            UpdateRotation();
        } else {
            if (jumpRoutine != null) {
                StopCoroutine(jumpRoutine);
                jumpRoutine = null;
            }
            HUD.HideHoldMouseIndicator();
            HUD.HideRequiredInstruments();
        }
        UpdateInteraction();
    }

    private void UpdateMovement() {
        motion = gi.VerticalAxis * transform.forward * speedMovement + gi.HorizontalAxis * transform.right * speedMovement;
        if (Physics.Raycast(new Ray(transform.position + motion * Time.deltaTime, -Vector3.up), out hitGround, walkableRaycastDist, walkableLayer) == false) {
            Debug.Log("PlayerController : Walkable Layer Not Detected - Setting Forward Motion to Zero");
            motion = Vector3.zero;
        } else {
            if (motion.magnitude != 0 && footsteps.isPlaying == false) {
                if (hitGround.collider.CompareTag("Grass")) {
                    footsteps.clip = footstepVariationsGrass[UnityEngine.Random.Range(0, footstepVariationsGrass.Length)];
                } else {
                    footsteps.clip = footstepVariationsStone[UnityEngine.Random.Range(0, footstepVariationsStone.Length)];
                }
                footsteps.Play();
            }
        }
        if (gi.Jump && cc.isGrounded) {
            if (jumpRoutine == null) jumpRoutine = StartCoroutine(DoJump());
        }
        motion += jumpAmount * Vector3.up;
        if (jumpRoutine == null) motion += Physics.gravity * gravityMultiplier;
        cc.Move(motion * Time.deltaTime);
    }

    public void Stop()
    {
        motion = Vector3.zero;
        footsteps.Stop();
        locked = true;
    }

    public void Resume()
    {
        locked = false;
    }

    private IEnumerator DoJump() {
        jump.clip = footstepVariationsGrass[UnityEngine.Random.Range(0, jumpVariations.Length)];
        jump.Play();
        float timeElapsed = 0;
        while(timeElapsed < jumpCurve[jumpCurve.length-1].time) {
            jumpAmount = jumpCurve.Evaluate(timeElapsed) * jumpSpeed;
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        if (footsteps.isPlaying == false) {
            if (hitGround.collider != null && hitGround.collider.CompareTag("Grass")) {
                footsteps.clip = footstepVariationsGrass[UnityEngine.Random.Range(0, footstepVariationsGrass.Length)];
            } else {
                footsteps.clip = footstepVariationsStone[UnityEngine.Random.Range(0, footstepVariationsStone.Length)];
            }
            footsteps.Play();
        }
        jumpRoutine = null;
    }

    private void UpdateRotation() {
        rotationX = Mathf.Clamp(rotationX - gi.MouseY * sensitivity * speedRotation * Time.deltaTime, -80, 80);
        neck.localRotation = Quaternion.Lerp(neck.localRotation, Quaternion.Euler(rotationX, 0f, 0f), sensitivity * speedRotation * Time.deltaTime);
        transform.Rotate(0f, gi.MouseX * sensitivity * speedRotation * Time.deltaTime, 0f);
    }

    private void UpdateInteraction() {
        if (Physics.Raycast(neck.position, neck.forward, out hit, interactRaycastDist, interactLayer)) { // QueryTriggerInteraction.Ignore
            if (hit.collider == null) return;
            if (hit.collider == interactedCollider) {
                if (Vector3.Distance(interactedCurrect.transform.position, transform.position) > interactedCurrect.InteractionDistance) {
                    interactedCurrect.EndInteract();
                    //Debug.Log("EndInteract() : " + interactedCurrect.name);
                    interactedCollider = null;
                    interactedCurrect = null;
                }
                return;
            }
            interactedTmp = hit.collider.GetComponent<Interact>();
            if (interactedTmp != interactedCurrect) {
                if (Vector3.Distance(interactedTmp.transform.position, transform.position) > interactedTmp.InteractionDistance) return;
                if (interactedCurrect != null) {
                    interactedCurrect.EndInteract();
                    //Debug.Log("EndInteract() : " + interactedCurrect.name);
                }
                interactedCurrect = interactedTmp;
                interactedCollider = hit.collider;
                interactedCurrect.BeginInteract();
                interactedPrevious = interactedCurrect;
                //Debug.Log("BeginInteract() : " + interactedCurrect.name);
            }
        } else {
            if (interactedCollider) {
                interactedCurrect.EndInteract();
                //Debug.Log("EndInteract() : " + interactedCurrect.name);
                interactedCollider = null;
                interactedCurrect = null;
            }
        }
    }

    private void UpdateInstruments() {
        if (interactedCurrect && interactedCurrect.enabled) {
            if (gi.MouseButtonLeft) {
                interactedCurrect.UpdateInteract();
                HUD.HideHoldMouseIndicator();
                AudioOnUpdateInteract((int)ic.Current, interactedCurrect.GetInstrumentValue(ic.Current), interactedCurrect.GetMelodyDuration());
            } else {
                interactedCurrect.PauseInteract();
                HUD.ShowHoldMouseIndicator();
                AudioOnPauseInteract((int)ic.Current, interactedCurrect.GetInstrumentValue(ic.Current), interactedCurrect.GetMelodyDuration());
            }
            Vector3 worldSpaceCanvasPos = transform.position + (interactedCollider.transform.position - transform.position).normalized * HUD_WorldSpace_DistMultiplier;
            worldSpaceCanvasPos.y = transform.position.y + HUD_WorldSpace_Height;
            HUD.ShowRequiredInstruments(worldSpaceCanvasPos, interactedCurrect.GetRequiredInstruments(), interactedCurrect.GetCurrentMelodyValues());
        } else {
            HUD.HideRequiredInstruments();
            HUD.HideHoldMouseIndicator();
            if (interactedPrevious) {
                AudioOnEndInteract((int)previousInstrument, interactedPrevious.GetInstrumentValue(previousInstrument), interactedPrevious.GetMelodyDuration());
                interactedPrevious = null;
            }
        }
        previousInstrument = ic.Current;
    }

    private void AudioOnUpdateInteract(int index, float value, float melodyDuration) {
        if (completeMelodyRoutine != null) return;
        //Debug.Log("AudioOnUpdateInteract " + (Enum.InstrumentType)index + " - " + value + " - " + melodyDuration);
        if (value >= melodyDuration) {
            return;
        };
        if(instrumentsPlay[index].isPlaying) {
            return;
        }
        if (value < 0) {
            AudioOnEndInteract((int)previousInstrument, interactedPrevious.GetInstrumentValue(previousInstrument), interactedPrevious.GetMelodyDuration());
            StopAllInstrumentsAudios();
            return;
        }
        switch (melodyDuration) {
            case 3:
                instrumentsPlay[index].clip = melody3Seconds[index]; 
                break;                                       
            case 5:                                          
                instrumentsPlay[index].clip = melody5Seconds[index]; 
                break;                                       
            case 7:                                          
                instrumentsPlay[index].clip = melody7Seconds[index]; 
                break;
            default: // do nothing
                break;
        }
        instrumentsPlay[index].time = value;
        instrumentsPlay[index].Play();
    }

    private void AudioOnPauseInteract(int index, float value, float melodyDuration) {
        if (completeMelodyRoutine != null) return;
        //Debug.Log("AudioOnPauseInteract " + (Enum.InstrumentType)index + " - " + value + " - " + melodyDuration);
        if (instrumentsPlay[index].isPlaying) {
            if (value < melodyDuration && value >= 0) {
                instrumentsPlay[index].Stop();
                PlayerFailInstrumentAudio(index, value, melodyDuration);
            }
        }
    }

    private void AudioOnEndInteract(int index, float value, float melodyDuration) {
        if (completeMelodyRoutine != null) return;
        //Debug.Log("AudioOnEndInteract " + (Enum.InstrumentType)index + " - " + value + " - " + melodyDuration);
        if (instrumentsPlay[index].isPlaying) {
            if (value < melodyDuration && value >= 0) {
                instrumentsPlay[index].Stop();
                PlayerFailInstrumentAudio(index, value, melodyDuration);
            }
        }
    }

    private void PlayerFailInstrumentAudio(int index, float value, float melodyDuration) {
        if (instrumentsFail[index].isPlaying) return;
        instrumentsFail[index].time = Mathf.Clamp(instrumentsFail[index].clip.length - instrumentsFail[index].clip.length * value / melodyDuration, 0, instrumentsFail[index].clip.length);
        instrumentsFail[index].Play();
    }

    private void StopAllInstrumentsAudios() {
        for (int i = 0; i < instrumentsPlay.Length; i++) {
            instrumentsPlay[i].Stop();
        }
    }

    public void PlayCompleteInstrumentAudio(Enum.InstrumentType[] requiredInstruments, float melodyDuration) {
        if (completeMelodyRoutine != null) return;
        completeMelodyRoutine = StartCoroutine(PlayCompleteInstrumentAudioRoutine(requiredInstruments, melodyDuration));
    }

    private IEnumerator PlayCompleteInstrumentAudioRoutine(Enum.InstrumentType[] requiredInstruments, float melodyDuration) {
        StopAllInstrumentsAudios();
        for (int i = 0; i < requiredInstruments.Length; i++) {
            instrumentsPlay[(int)requiredInstruments[i]].Play();
        }
        yield return new WaitForSeconds(melodyDuration);
        completeMelodyRoutine = null;
    }

    public void StartDialogue(Dialogue dialogue, Action dialogueCallback = null) {
        InDialogue = true;
        this.dialogueCallback = dialogueCallback;
        dc.ShowDialogue(dialogue, EndDialogue);
        ic.Hide();
        HUD.Hide();
    }

    private void EndDialogue() {
        if (dialogueCallback != null) dialogueCallback();
        dialogueCallback = null;
        InDialogue = false;
        ic.Show();
        HUD.Show();
    }

    public void OnBirdFound(int num) {
        birdsFound += num;
        HUD.OnBirdFound(birdsFound);
        if (birdFoundSFX.isPlaying == false)
            birdFoundSFX.Play();
        if (birdFlyingSFX.isPlaying == false)
            birdFlyingSFX.Play();
        if (birdsFound >= 30)
        {
            hoopoe.SetNewDialogue(HoopoeFinalDialogue, simorghSequenceStateController); 
        }
    }

    public void OnGameEnd()
    {
        // reload the game
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    private Coroutine writeTextRoutine;

    public void WriteText(string textToWrite, UnityEngine.UI.Text pLine)
    {
        if (writeTextRoutine != null)
        {
            StopCoroutine(writeTextRoutine);
            writeTextRoutine = null;
        }
        writeTextRoutine = StartCoroutine(WriteTextRoutine(textToWrite, pLine));
    }

    private IEnumerator WriteTextRoutine(string textToWrite, UnityEngine.UI.Text pLine)
    {
        int letterIndex = 1;
        while (letterIndex < textToWrite.Length)
        {
            pLine.text = textToWrite.Substring(0, letterIndex);
            letterIndex++;
            yield return new WaitForSeconds(Time.deltaTime * 0.1f);
        }
        pLine.text = textToWrite;
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, transform.position + motion * Time.deltaTime - Vector3.up * walkableRaycastDist);
        Gizmos.color = Color.yellow;
        if(neck) Gizmos.DrawLine(neck.position, neck.position + neck.forward * interactRaycastDist);
    }
}