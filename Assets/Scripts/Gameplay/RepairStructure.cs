using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using cakeslice;

public class RepairStructure : Interact
{
    [Serializable]
    public class Chunk
    {
        public Enum.InstrumentType requiredInstrument;
        [HideInInspector] public float repairValue;
        public Transform[] pieces;
        //public Vector3[] oldPositions; // set in Editor using Shatter Tool
        //public Quaternion[] oldRotations; // set in Editor using Shatter Tool
        [HideInInspector] public Vector3[] newPositions; // set in Awake
        [HideInInspector] public Quaternion[] newRotations; // set in Awake
        [HideInInspector] public Outline [] outlines;
    }

    [Header("Repair Structure")]
    [SerializeField] float melodyDuration;
    [SerializeField] GameObject completeStructure;

    public List<Chunk> chunks;

    [Header("Tree & Bush")]
    [SerializeField] SkinnedMeshRenderer[] blendShapeRenderers;

    [Header("Grass")]
    [SerializeField] Renderer[] grassRenderers;
    [SerializeField] int materialIndex = 0;
    [SerializeField] float maxGrassHeight = 1f;
    [SerializeField] float maxGrassSaturation = 3.5f;

    [Header("Color change")]
    [SerializeField] Renderer [] colorRenderers;
    [SerializeField] Color fromColor;
    [SerializeField] Color targetColor;

    [Header("State Controller")]
    [SerializeField] ObjectStateController stateController;

    public bool IsInitialized { get; private set; }
    public bool IsComplete { get; private set; }

    private Renderer[] chunks_transparent;
    private bool interacting;
    private bool usingInstrument;
    private Enum.InstrumentType[] requiredInstruments;
    private float[] melodyValues;
    private int _GrassThinness;
    private int _GrassSaturation;

    private void Awake() {
        _GrassThinness = Shader.PropertyToID("_GrassThinness");
        _GrassSaturation = Shader.PropertyToID("_GrassSaturation");
        requiredInstruments = new Enum.InstrumentType[chunks.Count];
        melodyValues = new float[chunks.Count];
        for (int i = 0; i < chunks.Count; i++) {
            requiredInstruments[i] = chunks[i].requiredInstrument;
            chunks[i].newPositions = new Vector3[chunks[i].pieces.Length];
            chunks[i].newRotations = new Quaternion[chunks[i].pieces.Length];
            chunks[i].outlines = new Outline[chunks[i].pieces.Length];
            for (int p = 0; p < chunks[i].pieces.Length; p++) {
                chunks[i].newPositions[p] = chunks[i].pieces[p].localPosition;
                chunks[i].newRotations[p] = chunks[i].pieces[p].localRotation;
            }

        }
        StartCoroutine(Initialize());
    }

    private IEnumerator Initialize() {
        yield return new WaitForEndOfFrame();
        chunks_transparent = new Renderer[GetTotalNumOfPiecesBeforeChunk(chunks.Count - 1) + chunks[chunks.Count - 1].pieces.Length];
        int totalNumOfPiecesBeforeThisChunk;
        for (int i = 0; i < chunks.Count; i++) {
            totalNumOfPiecesBeforeThisChunk = GetTotalNumOfPiecesBeforeChunk(i);
            for (int p = 0; p < chunks[i].pieces.Length; p++) {
                chunks_transparent[totalNumOfPiecesBeforeThisChunk + p] = Instantiate(chunks[i].pieces[p].gameObject, chunks[i].pieces[p].parent).GetComponent<Renderer>();
                chunks_transparent[totalNumOfPiecesBeforeThisChunk + p].shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                chunks_transparent[totalNumOfPiecesBeforeThisChunk + p].receiveShadows = false;
                chunks_transparent[totalNumOfPiecesBeforeThisChunk + p].enabled = false;
                chunks_transparent[totalNumOfPiecesBeforeThisChunk + p].material = GameManager.Instance.GetPlayerController().TransparentMaterial;
                chunks[i].outlines[p] = chunks[i].pieces[p].gameObject.AddComponent<Outline>();
                chunks[i].outlines[p].enabled = false;
                yield return new WaitForEndOfFrame();
            }
        }
        IsInitialized = true;
    }

    public override void BeginInteract() {
        interacting = true;
    }

    public override void UpdateInteract() {
        usingInstrument = true;
    }

    public override void PauseInteract() {
        usingInstrument = false;
    }

    public override void EndInteract() {
        interacting = false;
        usingInstrument = false;
    }

    public override float GetMelodyDuration() {
        return melodyDuration;
    }

    public override Enum.InstrumentType [] GetRequiredInstruments() {
        return requiredInstruments;
    }

    public override float [] GetCurrentMelodyValues() {
        return melodyValues;
    }

    public override float GetInstrumentValue(Enum.InstrumentType instrument) {
        if(chunks == null) {
            return melodyDuration;
        }
        for (int i = 0; i < chunks.Count; i++) {
            if (chunks[i].requiredInstrument == instrument) {
                if (chunks[i].repairValue < melodyDuration) {
                    return chunks[i].repairValue;
                }
            }
        }
        return -1;
    }

    private void Update() {
        if (IsInitialized == false) return;
        Enum.InstrumentType instrument = GameManager.Instance.GetPlayerController().CurrentInstrument;
        int totalNumOfPiecesBeforeThisChunk;
        bool chunksFixed = true;
        for (int i = 0; i < chunks.Count; i++) {
            if (chunks[i].repairValue < melodyDuration) {
                if (interacting) {
                    if (chunks[i].requiredInstrument == instrument) {
                        AddOutline(i, 0);
                    } else {
                        AddOutline(i, 1);
                    }
                } else {
                    RemoveOutline(i);
                }
                if (usingInstrument && chunks[i].requiredInstrument == instrument) {
                    chunks[i].repairValue = Mathf.Clamp(chunks[i].repairValue + Time.deltaTime, 0f, melodyDuration);
                } else {
                    if (chunks[i].repairValue > 0) chunks[i].repairValue = Mathf.Clamp(chunks[i].repairValue - Time.deltaTime * 6, 0f, melodyDuration);
                }
                totalNumOfPiecesBeforeThisChunk = GetTotalNumOfPiecesBeforeChunk(i);
                for (int p = 0; p < chunks[i].pieces.Length; p++) {
                    if (chunks[i].repairValue > 0) {
                        chunks_transparent[totalNumOfPiecesBeforeThisChunk + p].transform.localPosition = Vector3.Lerp(chunks[i].newPositions[p], Vector3.zero, chunks[i].repairValue / melodyDuration);
                        chunks_transparent[totalNumOfPiecesBeforeThisChunk + p].transform.localRotation = Quaternion.Lerp(chunks[i].newRotations[p], Quaternion.identity, chunks[i].repairValue / melodyDuration);
                    }
                    chunks_transparent[totalNumOfPiecesBeforeThisChunk + p].enabled = chunks[i].repairValue > 0;
                }
                chunksFixed = false;
            } else {
                RemoveOutline(i);
            }
            melodyValues[i] = chunks[i].repairValue / melodyDuration;
        }
        if (chunksFixed) {
            GameManager.Instance.StartCoroutine(CompleteStructure());
            GameManager.Instance.GetPlayerController().PlayCompleteInstrumentAudio(requiredInstruments, melodyDuration);
            enabled = false;
        }
    }

    private int GetTotalNumOfPiecesBeforeChunk(int chunkIndex) {
        int totalNum = 0;
        for(int i = 0; i < chunks.Count; i++) {
            if (chunkIndex == i) return totalNum;
            totalNum += chunks[i].pieces.Length;
        }
        return totalNum;
    }

    private IEnumerator CompleteStructure() {
        float repairValue = 0;
        for (int i = 0; i < chunks.Count; i++) {
            RemoveOutline(i);
        }
        while (repairValue < melodyDuration) {
            repairValue = Mathf.Clamp(repairValue + Time.deltaTime, 0f, melodyDuration);
            for (int i = 0; i < chunks.Count; i++) {
                for (int p = 0; p < chunks[i].pieces.Length; p++) {
                    chunks[i].pieces[p].localPosition = Vector3.Lerp(chunks[i].newPositions[p], Vector3.zero, repairValue / melodyDuration);
                    chunks[i].pieces[p].localRotation = Quaternion.Lerp(chunks[i].newRotations[p], Quaternion.identity, repairValue / melodyDuration);
                }
            }
            if (grassRenderers != null && grassRenderers.Length != 0) {
                for (int i = 0; i < grassRenderers.Length; i++) {
                    grassRenderers[i].materials[materialIndex].SetFloat(_GrassThinness, maxGrassHeight * Mathf.Clamp01(repairValue / melodyDuration));
                    grassRenderers[i].materials[materialIndex].SetFloat(_GrassSaturation, maxGrassSaturation * Mathf.Clamp01(repairValue / melodyDuration));
                }
            }
            if (blendShapeRenderers != null && blendShapeRenderers.Length != 0) {
                for (int i = 0; i < blendShapeRenderers.Length; i++) {
                    blendShapeRenderers[i].SetBlendShapeWeight(0, 100f * Mathf.Clamp01(repairValue / melodyDuration));
                }
            }
            if (colorRenderers != null && colorRenderers.Length != 0) {
                for (int i = 0; i < colorRenderers.Length; i++) {
                    colorRenderers[i].material.color = Color.Lerp(fromColor, targetColor, Mathf.Clamp01(repairValue / melodyDuration));
                }
            }
            yield return null;
        }
        if (completeStructure) {
            completeStructure.SetActive(true);
            DestoryChunks();
        }
        IsComplete = true;
        ActivateStateController();
    }

    public void DestoryChunks() {
        int totalNumOfPiecesBeforeThisChunk;
        for (int i = 0; i < chunks.Count; i++) {
            totalNumOfPiecesBeforeThisChunk = GetTotalNumOfPiecesBeforeChunk(i);
            for (int p = 0; p < chunks[i].pieces.Length; p++) {
                Destroy(chunks[i].pieces[p].gameObject);
                Destroy(chunks_transparent[totalNumOfPiecesBeforeThisChunk + p].gameObject);
            }
        }
        chunks_transparent = null;
        chunks = null;
    }

    private void ActivateStateController() {
        if (stateController != null) {
            stateController.Activate();
        }
    }

    public void AddOutline(int chunkIndex, int outlineIndex) {
        for (int p = 0; p < chunks[chunkIndex].pieces.Length; p++) {
            chunks[chunkIndex].outlines[p].enabled = true;
            chunks[chunkIndex].outlines[p].color = outlineIndex;
        }
    }

    public void RemoveOutline(int chunkIndex) {
        for (int p = 0; p < chunks[chunkIndex].pieces.Length; p++) {
            chunks[chunkIndex].outlines[p].enabled = false;
        }
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, InteractionDistance);
    }
}
