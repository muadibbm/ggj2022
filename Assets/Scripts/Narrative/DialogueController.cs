﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class DialogueController : UI_Panel
{
    [SerializeField] UI_Panel cinematicPanel;
    [SerializeField] Text line;

    [SerializeField] AudioSource birdTalk;
    [SerializeField] AudioClip[] birdTalkVariation;

    private GameInput gi;
    private Dialogue dialogue;
    private int currentLineIndex;
    private bool firstLine;
    private Action endDialogueCallback;
    private Coroutine writeTextRoutine;
    private int birdTalkVariationIndex;

    protected override void Awake() {
        base.Awake();
        Hide();
        gi = GameManager.Instance.GetTool<GameInput>("GameInput");
    }

    public void ShowDialogue(Dialogue dialogue, Action endDialogueCallback) {
        if (this.dialogue != null) return;
        this.dialogue = dialogue;
        currentLineIndex = 0;
        firstLine = true;
        line.text = string.Empty;
        cinematicPanel.Show();
        this.endDialogueCallback = endDialogueCallback;
        ProcessDialogue();
        Show();
    }

    private void Update() {
        if (dialogue == null) return;
        if(gi.MouseButtonLeftDown && firstLine == false) {
            ProcessDialogue();
        }
    }

    public override void Hide() {
        if (writeTextRoutine != null) {
            StopCoroutine(writeTextRoutine);
            writeTextRoutine = null;
        }
        base.Hide();
    }

    public void ProcessDialogue() {
        if (firstLine == false) {
            if (currentLineIndex != dialogue.lines.Length - 1) {
                currentLineIndex++;
            } else {
                // end of dialouge
                Hide();
                cinematicPanel.Hide();
                endDialogueCallback();
                dialogue = null;
                return;
            }
        }
        firstLine = false;
        if(dialogue != null) WriteText();
        // TODO : write onto screen
    }

    private void WriteText() {
        if (birdTalk.isPlaying) birdTalk.Stop();
        birdTalk.clip = birdTalkVariation[birdTalkVariationIndex];
        birdTalkVariationIndex++;
        if (birdTalkVariationIndex > birdTalkVariation.Length - 1) birdTalkVariationIndex = 0;
        birdTalk.Play();
        GameManager.Instance.GetPlayerController().WriteText(dialogue.lines[currentLineIndex], line);
    }
}