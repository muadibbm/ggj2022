using UnityEngine;

public class InstrumentController : MonoBehaviour
{
    [SerializeField] Enum.InstrumentType current;
    [SerializeField] Transform [] instruments;
    [SerializeField] float speedSwitch;

    public Enum.InstrumentType Current { get { return current; } }

    private GameInput gi;
    private int lastIndex;
    private Vector3[] equipped_positions;
    private Quaternion[] equipped_rotations;
    private float [] equipped_Value;
    private bool isVisible;

    private void Awake() {
        gi = GameManager.Instance.GetTool<GameInput>("GameInput");
        lastIndex = System.Enum.GetNames(typeof(Enum.InstrumentType)).Length-1;
        equipped_positions = new Vector3[instruments.Length];
        equipped_rotations = new Quaternion[instruments.Length];
        equipped_Value = new float[instruments.Length];
        for (int i = 0; i < instruments.Length; i++) {
            equipped_positions[i] = instruments[i].localPosition;
            equipped_rotations[i] = instruments[i].localRotation;
        }
    }
    
    public void Show() {
        isVisible = true;
    }

    public void Hide() {
        isVisible = false;
    }

    private void Update() {
        if (Mathf.Abs(gi.MouseScrollDelta.y) > 0.5f) {
            SwitchInstrument();
        }
        for (int i = 0; i < instruments.Length; i++) {
            equipped_Value[i] = Mathf.Clamp01(equipped_Value[i] + Time.deltaTime * (isVisible && (current == (Enum.InstrumentType)i) ? speedSwitch : -speedSwitch));
            instruments[i].localPosition = Vector3.Lerp(Vector3.zero, equipped_positions[i], equipped_Value[i]);
            instruments[i].localRotation = Quaternion.Lerp(Quaternion.identity, equipped_rotations[i], equipped_Value[i]);
            instruments[i].gameObject.SetActive(equipped_Value[i] != 0f);
        }
    }

    private void SwitchInstrument() {
        if (gi.MouseScrollDelta.y > 0) {
            if ((int)current == lastIndex) {
                current = 0;
                return;
            }
        } else {
            if ((int)current == 0) {
                current = (Enum.InstrumentType)lastIndex;
                return;
            }
        }
        current += (gi.MouseScrollDelta.y > 0) ? 1 : -1;
    }
}
