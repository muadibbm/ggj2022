﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class ShatterToolUtility : EditorWindow {

    private List<GameObject> gameObjects = new List<GameObject>(1);
    private Transform shatterPlanesParents;
    private bool shatterToolkitPluginMethod;
    private int numOfGos = 1;
    private int numOfCuts = 1;
    private Transform[] shatterPlanes;

    [MenuItem("Tools/Shatter Tool Utility")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        ShatterToolUtility window = (ShatterToolUtility)GetWindow(typeof(ShatterToolUtility));
        window.Show();
    }

    void OnGUI() {
        numOfGos = gameObjects.Count;
        numOfGos = EditorGUILayout.DelayedIntField("Number of Game Objects", numOfGos);

        if (numOfGos != gameObjects.Count) {
            while (numOfGos > gameObjects.Count) {
                gameObjects.Insert(gameObjects.Count, null);
            }
            while (numOfGos < gameObjects.Count) {
                gameObjects.RemoveAt(gameObjects.Count - 1);
            }
        }
        if (gameObjects != null && gameObjects.Count > 0) {
            for (int i = 0; i < gameObjects.Count; i++) {
                gameObjects[i] = (GameObject)EditorGUILayout.ObjectField("Game Object " + i + " : ", gameObjects[i], typeof(GameObject), true);
            }
        }
        shatterToolkitPluginMethod = EditorGUILayout.Toggle("Use Shatter Toolkit Plugin : ", shatterToolkitPluginMethod);
        numOfCuts = EditorGUILayout.IntField("Number of Cuts : ", numOfCuts);

        if (shatterToolkitPluginMethod) {
            shatterPlanesParents = (Transform)EditorGUILayout.ObjectField("Shatter Planes Parent : ", shatterPlanesParents, typeof(Transform), true);
        }

        if (GUILayout.Button("Shatter Mesh")) {
            if (gameObjects != null && gameObjects.Count > 0) {

                shatterPlanes = new Transform[shatterPlanesParents.childCount];
                int index = 0;
                foreach (Transform child in shatterPlanesParents) {
                    shatterPlanes[index] = child;
                    index++;
                }

                for (int i = 0; i < gameObjects.Count; i++) {
                    if (shatterToolkitPluginMethod) {
                        if (shatterPlanes != null && shatterPlanes.Length > 0) {
                            ShatterUsingTool(gameObjects[i]);
                        }
                    } else {
                        ShatterManual(gameObjects[i], gameObjects[i].GetComponent<Renderer>(), gameObjects[i].GetComponent<MeshRenderer>(),
                            gameObjects[i].GetComponent<SkinnedMeshRenderer>(), gameObjects[i].GetComponent<MeshFilter>(), gameObjects[i].GetComponent<Collider>());
                    }
                }
            }
        }
    }

    private void ShatterUsingTool(GameObject go) {
        ShatterToolkit.ShatterTool shatterTool = go.AddComponent<ShatterToolkit.ShatterTool>();
        ShatterToolkit.WorldUvMapper worldUVMapper = go.AddComponent<ShatterToolkit.WorldUvMapper>();
        shatterTool.Start();
        shatterTool.Cuts = numOfCuts;
        Plane[] planes = new Plane[shatterPlanes.Length];
        for (int i = 0; i < shatterPlanes.Length; i++) {
            planes[i] = new Plane(shatterPlanes[i].up, shatterPlanes[i].position);
        }
        shatterTool.Split(planes, false);
        DestroyImmediate(shatterTool);
        DestroyImmediate(worldUVMapper);
    }

    private void ShatterManual(GameObject go, Renderer rend, MeshRenderer mRend, SkinnedMeshRenderer sRend, MeshFilter mFilter, Collider col) {

        if (mFilter == null && sRend == null) {
            return;
        }

        if (col) {
            col.enabled = false;
        }

        Mesh M = new Mesh();
        if (mFilter) {
            M = mFilter.sharedMesh;
        } else if (sRend) {
            M = sRend.sharedMesh;
        }

        List<Material> materials = new List<Material>();
        if (mRend) {
            mRend.GetMaterials(materials);
        } else if (sRend) {
            sRend.GetMaterials(materials);
        }

        Vector3[] verts = M.vertices;
        Vector3[] normals = M.normals;
        Vector2[] uvs = M.uv;
        for (int submesh = 0; submesh < M.subMeshCount; submesh++) {

            int[] indices = M.GetTriangles(submesh);

            int m = 3;
            for (int i = 0; i < indices.Length; i += m) {
                Vector3[] newVerts = new Vector3[m];
                Vector3[] newNormals = new Vector3[m];
                Vector2[] newUvs = new Vector2[m];
                for (int n = 0; n < m; n++) {
                    int index = indices[i + n];
                    newVerts[n] = verts[index];
                    newUvs[n] = uvs[index];
                    newNormals[n] = normals[index];
                }

                Mesh mesh = new Mesh();
                mesh.vertices = newVerts;
                mesh.normals = newNormals;
                mesh.uv = newUvs;

                mesh.triangles = new int[] { 0, 1, 2, 2, 1, 0 };

                GameObject GO = new GameObject(go.name + "_piece_" + (i / 3));
                GO.transform.position = go.transform.position;
                GO.transform.rotation = go.transform.rotation;
                GO.AddComponent<MeshRenderer>().material = materials[submesh];
                GO.AddComponent<MeshFilter>().mesh = mesh;
                GO.transform.parent = go.transform;
            }
        }
    }

}
