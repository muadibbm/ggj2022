using UnityEngine;
using System.Collections;

public class BirdFly : MonoBehaviour
{
    [SerializeField] Transform destination;
    [SerializeField] float flyingSpeed_base = 2f;
    [SerializeField] float flyingSpeed = 4f;
    [SerializeField] float duration;
    [SerializeField] float durationToCorrectRotation;
    [SerializeField] float verticalHeight;

    private Transform target;
    private Animator anim;

    private void Awake() {
        anim = GetComponentInChildren<Animator>();
        anim.Play("Idle", -1, Random.Range(0f, 0.9f));
    }

    private void OnTriggerEnter(Collider other) {
        target = destination;
        Destroy(GetComponent<Collider>());
        GameManager.Instance.GetPlayerController().OnBirdFound(1);
        StartCoroutine(MoveToLocation());
    }

    public IEnumerator MoveToLocation() {
        yield return new WaitForSeconds(Random.Range(0, 1f));
        Vector3 fromPos = transform.position;
        Vector3 toPos = target.position;
        Vector3 prevPostion = transform.position;
        float timeElapsed = 0;
        float _timeToRotate = 0;
        anim.SetTrigger("Fly");
        while (timeElapsed < duration) {
            anim.SetFloat("Speed", flyingSpeed_base + flyingSpeed * _timeToRotate / durationToCorrectRotation);
            transform.position = Vector3.Lerp(fromPos,
                    toPos + verticalHeight * Mathf.Sin(Mathf.PI * timeElapsed / duration) * Vector3.up, timeElapsed / duration);
            transform.forward = (transform.position - prevPostion).normalized;
            timeElapsed += Time.deltaTime;
            _timeToRotate = Mathf.Clamp(_timeToRotate + Time.deltaTime, 0, durationToCorrectRotation);
            prevPostion = transform.position;
            yield return null;
        }
        destination.gameObject.SetActive(true);
        Destroy(gameObject);
    }
}
