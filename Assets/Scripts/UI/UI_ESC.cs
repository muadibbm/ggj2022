﻿using UnityEngine;

public class UI_ESC : MonoBehaviour
{
    [SerializeField] GameObject escMenu;
    private GameInput gi;
    private bool isVisible;

    private void Awake() {
        gi = GameManager.Instance.GetTool<GameInput>("GameInput");
        isVisible = false;
    }

    private void Update() {
        if(gi.Quit) {
            isVisible = !isVisible;
            escMenu.SetActive(isVisible);
            Cursor.visible = isVisible;
            Time.timeScale = (isVisible) ? 0 : 1;
            if(isVisible) GameManager.Instance.GetPlayerController().Stop();
            else GameManager.Instance.GetPlayerController().Resume();
        }
    }

    public void OnResume()
    {
        isVisible = false;
        escMenu.SetActive(false);
        Cursor.visible = false;
        Time.timeScale = 1;
        GameManager.Instance.GetPlayerController().Resume();
    }

    public void ApplicationQuit() {
        Application.Quit();
    }
}
