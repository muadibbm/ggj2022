using UnityEngine;

public class RepairStructureChecker : MonoBehaviour
{
    [SerializeField] RepairStructure[] repairStructures;
    [SerializeField] ObjectStateController stateController;

    private void Update() {
        for (int i = 0; i < repairStructures.Length; i++) {
            if (repairStructures[i].IsComplete == false) return;
        }
        stateController.Activate();
        DestoryChunks();
        enabled = false;
    }

    private void DestoryChunks() {
        for (int i = 0; i < repairStructures.Length; i++)
            repairStructures[i].DestoryChunks();
    }
}
