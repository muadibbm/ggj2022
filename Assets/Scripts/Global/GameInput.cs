﻿using UnityEngine;

public class GameInput : MonoBehaviour
{
    public bool AnyKeyDown { get; private set; }
    public bool MouseButtonLeft { get; private set; }
    public bool MouseButtonRight { get; private set; }
    public bool MouseButtonLeftDown { get; private set; }
    public bool MouseButtonRightDown { get; private set; }
    public Vector2 MousePosition { get; private set; }
    public Vector2 MouseScrollDelta { get; private set; }
    public float MouseX { get; private set; }
    public float MouseY { get; private set; }
    public float HorizontalAxis { get; private set; }
    public float VerticalAxis { get; private set; }
    public bool Jump { get; private set; }
    public bool Quit { get; private set; }

    public void SetCursorVisibility(bool val) {
        Cursor.visible = val;
    }

    private void Update() {
        AnyKeyDown = Input.anyKeyDown;
        MouseButtonLeft = Input.GetMouseButton(0);
        MouseButtonRight = Input.GetMouseButton(1);
        MouseButtonLeftDown = Input.GetMouseButtonDown(0);
        MouseButtonRightDown = Input.GetMouseButtonDown(1);
        MousePosition = Input.mousePosition;
        MouseScrollDelta = Input.mouseScrollDelta;
        HorizontalAxis = Input.GetAxis("Horizontal");
        VerticalAxis = Input.GetAxis("Vertical");
        MouseX = Input.GetAxis("Mouse X");
        MouseY = Input.GetAxis("Mouse Y");
        Jump = Input.GetKeyDown(KeyCode.Space);
        Quit = Input.GetKeyDown(KeyCode.Escape);
    }
}