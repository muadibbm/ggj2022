// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Stylized/Water"
{
	Properties
	{
		[Header(Global Variables)]_DistanceFade("Distance Fade", Float) = 40
		_Alpha("Alpha", Float) = 1
		_DepthFade("Depth Fade", Float) = 20
		[HDR][Header(Base Color)]_Shallow("Shallow", Color) = (0.09448972,0.9691254,0.2277445,1)
		_Deep("Deep", Color) = (0,0.05,0.195,1)
		_Far("Far", Color) = (0,0.253,0.785,1)
		[Header(Edge Foam)][Toggle(_EDGEFOAMENABLED_ON)] _EdgeFoamEnabled("Edge Foam Enabled", Float) = 0
		_EdgeFoamDepth("Edge Foam Depth", Float) = 1
		_EdgeFoamNoiseScale("Edge Foam Noise Scale", Float) = 1
		_EdgeFoamColor("Edge Foam Color", Color) = (1,1,1,1)
		[Header(Sun Specular)][Toggle]_SunSpecularEnabled("Sun Specular Enabled", Float) = 1
		_SunSpecularExponent("Sun Specular Exponent", Float) = 2000
		_SunSpecularColor("Sun Specular Color", Color) = (1,1,1,1)
		[Header(Sparkle)][Toggle(_SPARKLEENABLED_ON)] _SparkleEnabled("Sparkle Enabled", Float) = 0
		[NoScaleOffset][Normal]_SparkleNormalMap("Sparkle Normal Map", 2D) = "bump" {}
		_SparkleScale("Sparkle Scale", Float) = 50
		_SparkleSpeed("Sparkle Speed", Float) = 0.75
		_SparkleExponent("Sparkle Exponent", Float) = 500
		_SparkleColor("Sparkle Color", Color) = (1,1,1,1)
		[Header(Wave Normal Map)][Toggle(_WAVENORMALENABLED_ON)] _WaveNormalEnabled("Wave Normal Enabled", Float) = 1
		[NoScaleOffset][Normal]_WaveNormal("Wave Normal", 2D) = "bump" {}
		_WaveNormalScale("Wave Normal  Scale", Float) = 50
		_WaveNormalSpeed("Wave Normal Speed", Float) = 0.25
		[Header(Refraction)]_RefractionContribution("Refraction Contribution", Range( 0 , 1)) = 1
		[Toggle]_ModulateRefractionbyDepth("Modulate Refraction by Depth", Float) = 1
		[Header(Foam)][Toggle(_FOAMENABLED_ON)] _FoamEnabled("Foam Enabled", Float) = 0
		[NoScaleOffset]_Foam("Foam", 2D) = "black" {}
		_FoamScale("Foam Scale", Float) = 1
		_FoamSpeed("Foam Speed", Float) = 1
		_FoamHeightOffset("Foam Height Offset", Float) = 0
		_FoamHeightMultiplier("Foam Height Multiplier", Float) = 1
		_FoamNoiseScale("Foam Noise Scale", Range( 0 , 1)) = 1
		[Header(Subsurface Scattering)][Toggle]_SSSEnabled("SSS Enabled", Float) = 1
		_SSSColor("SSS Color", Color) = (1,1,1,0)
		[Header(Reflection)][Toggle(_REFLECTIONENABLED_ON)] _ReflectionEnabled("Reflection Enabled", Float) = 1
		[NoScaleOffset]_ReflectionCubemap("Reflection Cubemap", CUBE) = "black" {}

		/*[Header(Wave 1)]_WaveDirection1("Wave Direction 1", Range( 0 , 1)) = 0
		_Wavelength1("Wavelength 1", Float) = 1
		_Speed1("Speed 1", Float) = 1
		_Amplitude1("Amplitude 1", Float) = 1
		[Header(Wave 2)]_WaveDirection2("Wave Direction 2", Range( 0 , 1)) = 0
		_Wavelength2("Wavelength 2", Float) = 1
		_Speed2("Speed 2", Float) = 1
		_Amplitude2("Amplitude 2", Float) = 1*/

		/*
		[Header(Wave 1)]_WaveDirection1("Wave Direction 1", Range( 0 , 1)) = 0
		[Header(Wave 2)]_WaveDirection2("Wave Direction 2", Range( 0 , 1)) = 0
		_Wavelength2("Wavelength 2", Float) = 1
		_Speed2("Speed 2", Float) = 1
		_Amplitude2("Amplitude 2", Float) = 1

		_WaveSourceX("WaveSourceX", Float) = 1
		_WaveSourceZ("WaveSourceZ", Float) = 1
		_Wavelength1("Wavelength 1", Float) = 1
		_Speed1("Speed 1", Float) = 1
		_Amplitude1("Amplitude 1", Float) = 1
		*/
		_TimeValue("_Time", Float) = 0
		_WaveSourceX("WaveSourceX", Float) = 3000
		_WaveSourceZ("WaveSourceZ", Float) = 3000
		_Wavelength1("Wavelength 1", Float) = 1
		_Speed1("Speed 1", Float) = 1
		_Amplitude1("Amplitude 1", Float) = 1

		[Header(Wave 1)]_WaveDirection1("Wave Direction 1", Range( 0 , 1)) = 0
		[Header(Wave 2)]_WaveDirection2("Wave Direction 2", Range( 0 , 1)) = 0
		_Wavelength2("Wavelength 2", Float) = 1
		_Speed2("Speed 2", Float) = 1
		_Amplitude2("Amplitude 2", Float) = 1

		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "ForceNoShadowCasting" = "True" "IsEmissive" = "true"  }
		Cull Back
		GrabPass{ }
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "UnityStandardUtils.cginc"
		#pragma target 3.0
		#pragma shader_feature _WAVENORMALENABLED_ON
		#pragma shader_feature _REFLECTIONENABLED_ON
		#pragma shader_feature _FOAMENABLED_ON
		#pragma shader_feature _SPARKLEENABLED_ON
		#pragma shader_feature _EDGEFOAMENABLED_ON
		#pragma surface surf Unlit alpha:fade keepalpha nodynlightmap nodirlightmap vertex:vertexDataFunc 
		struct Input
		{
			float3 worldPos;
			float4 screenPos;
			half eyeDepth;
			half vertexToFrag11_g101;
			INTERNAL_DATA
			float3 worldNormal;
			float3 worldRefl;
			half vertexToFrag11_g103;
			half vertexToFrag11_g105;
		};

		uniform half _TimeValue;
		uniform half _WaveSourceX;
		uniform half _WaveSourceZ;
		uniform half _Speed1;
		uniform half _WaveDirection1;
		uniform half _Wavelength1;
		uniform half _Amplitude1;
		uniform half _Speed2;
		uniform half _WaveDirection2;
		uniform half _Wavelength2;
		uniform half _Amplitude2;
		uniform half4 _Shallow;
		uniform sampler2D _GrabTexture;
		uniform half _RefractionContribution;
		uniform half _ModulateRefractionbyDepth;
		UNITY_DECLARE_DEPTH_TEXTURE( _CameraDepthTexture );
		uniform float4 _CameraDepthTexture_TexelSize;
		uniform half _DepthFade;
		uniform half _Alpha;
		uniform sampler2D _WaveNormal;
		uniform half _WaveNormalSpeed;
		uniform half _WaveNormalScale;
		uniform half4 _Deep;
		uniform half4 _Far;
		uniform half _DistanceFade;
		uniform half _SSSEnabled;
		uniform half4 _SSSColor;
		uniform samplerCUBE _ReflectionCubemap;
		uniform half _FoamHeightMultiplier;
		uniform half _FoamHeightOffset;
		uniform sampler2D _Foam;
		uniform half _FoamSpeed;
		uniform half _FoamScale;
		uniform half _FoamNoiseScale;
		uniform half4 _SparkleColor;
		uniform sampler2D _SparkleNormalMap;
		uniform half _SparkleSpeed;
		uniform half _SparkleScale;
		uniform half _SparkleExponent;
		uniform half4 _SunSpecularColor;
		uniform half _SunSpecularEnabled;
		uniform half _SunSpecularExponent;
		uniform half4 _EdgeFoamColor;
		uniform half _EdgeFoamDepth;
		uniform half _EdgeFoamNoiseScale;


		inline float4 ASE_ComputeGrabScreenPos( float4 pos )
		{
			#if UNITY_UV_STARTS_AT_TOP
			float scale = -1.0;
			#else
			float scale = 1.0;
			#endif
			float4 o = pos;
			o.y = pos.w * 0.5f;
			o.y = ( pos.y - o.y ) * _ProjectionParams.x * scale + o.y;
			return o;
		}


		float3 mod3D289( float3 x ) { return x - floor( x / 289.0 ) * 289.0; }

		float4 mod3D289( float4 x ) { return x - floor( x / 289.0 ) * 289.0; }

		float4 permute( float4 x ) { return mod3D289( ( x * 34.0 + 1.0 ) * x ); }

		float4 taylorInvSqrt( float4 r ) { return 1.79284291400159 - r * 0.85373472095314; }

		float snoise( float3 v )
		{
			const float2 C = float2( 1.0 / 6.0, 1.0 / 3.0 );
			float3 i = floor( v + dot( v, C.yyy ) );
			float3 x0 = v - i + dot( i, C.xxx );
			float3 g = step( x0.yzx, x0.xyz );
			float3 l = 1.0 - g;
			float3 i1 = min( g.xyz, l.zxy );
			float3 i2 = max( g.xyz, l.zxy );
			float3 x1 = x0 - i1 + C.xxx;
			float3 x2 = x0 - i2 + C.yyy;
			float3 x3 = x0 - 0.5;
			i = mod3D289( i);
			float4 p = permute( permute( permute( i.z + float4( 0.0, i1.z, i2.z, 1.0 ) ) + i.y + float4( 0.0, i1.y, i2.y, 1.0 ) ) + i.x + float4( 0.0, i1.x, i2.x, 1.0 ) );
			float4 j = p - 49.0 * floor( p / 49.0 );  // mod(p,7*7)
			float4 x_ = floor( j / 7.0 );
			float4 y_ = floor( j - 7.0 * x_ );  // mod(j,N)
			float4 x = ( x_ * 2.0 + 0.5 ) / 7.0 - 1.0;
			float4 y = ( y_ * 2.0 + 0.5 ) / 7.0 - 1.0;
			float4 h = 1.0 - abs( x ) - abs( y );
			float4 b0 = float4( x.xy, y.xy );
			float4 b1 = float4( x.zw, y.zw );
			float4 s0 = floor( b0 ) * 2.0 + 1.0;
			float4 s1 = floor( b1 ) * 2.0 + 1.0;
			float4 sh = -step( h, 0.0 );
			float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
			float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
			float3 g0 = float3( a0.xy, h.x );
			float3 g1 = float3( a0.zw, h.y );
			float3 g2 = float3( a1.xy, h.z );
			float3 g3 = float3( a1.zw, h.w );
			float4 norm = taylorInvSqrt( float4( dot( g0, g0 ), dot( g1, g1 ), dot( g2, g2 ), dot( g3, g3 ) ) );
			g0 *= norm.x;
			g1 *= norm.y;
			g2 *= norm.z;
			g3 *= norm.w;
			float4 m = max( 0.6 - float4( dot( x0, x0 ), dot( x1, x1 ), dot( x2, x2 ), dot( x3, x3 ) ), 0.0 );
			m = m* m;
			m = m* m;
			float4 px = float4( dot( x0, g0 ), dot( x1, g1 ), dot( x2, g2 ), dot( x3, g3 ) );
			return 42.0 * dot( m, px);
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );

			float dist = sqrt(pow(ase_worldPos.x - _WaveSourceX, 2) + pow(ase_worldPos.z - _WaveSourceZ, 2));
			dist = (dist % _Wavelength1) / _Wavelength1;
			v.vertex.y = _Amplitude1 * sin(2 * UNITY_PI * (_TimeValue * _Speed1 + dist));

			o.eyeDepth = -UnityObjectToViewPos( v.vertex.xyz ).z;
			o.vertexToFrag11_g101 = ( 1.0 - exp2( -( distance( _WorldSpaceCameraPos , ase_worldPos ) / _DistanceFade ) ) );
			o.vertexToFrag11_g103 = ( 1.0 - exp2( -( distance( _WorldSpaceCameraPos , ase_worldPos ) / _DistanceFade ) ) );
			o.vertexToFrag11_g105 = ( 1.0 - exp2( -( distance( _WorldSpaceCameraPos , ase_worldPos ) / _DistanceFade ) ) );
		}

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			o.Normal = float3(0,0,1);
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_grabScreenPos = ASE_ComputeGrabScreenPos( ase_screenPos );
			float4 ase_grabScreenPosNorm = ase_grabScreenPos / ase_grabScreenPos.w;
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float screenDepth532 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD( ase_screenPos ))));
			float distanceDepth532 = abs( ( screenDepth532 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( _DepthFade ) );
			float mulTime6_g48 = _TimeValue * _WaveNormalSpeed;
			float Time23_g48 = mulTime6_g48;
			float3 ase_worldPos = i.worldPos;
			float2 appendResult3_g47 = (half2(ase_worldPos.x , ase_worldPos.z));
			float2 Coordinates22_g48 = ( appendResult3_g47 / _WaveNormalScale );
			float2 panner14_g48 = ( Time23_g48 * half2( 0.1,0.1 ) + Coordinates22_g48);
			float2 panner15_g48 = ( Time23_g48 * half2( -0.1,-0.1 ) + ( Coordinates22_g48 + half2( 0.418,0.355 ) ));
			float2 panner16_g48 = ( Time23_g48 * half2( -0.1,0.1 ) + ( Coordinates22_g48 + half2( 0.865,0.148 ) ));
			float2 panner17_g48 = ( Time23_g48 * half2( 0.1,-0.1 ) + ( Coordinates22_g48 + half2( 0.651,0.752 ) ));
			#ifdef _WAVENORMALENABLED_ON
				float3 staticSwitch310 = ( ( UnpackNormal( tex2D( _WaveNormal, panner14_g48 ) ) + UnpackNormal( tex2D( _WaveNormal, panner15_g48 ) ) + UnpackNormal( tex2D( _WaveNormal, panner16_g48 ) ) + UnpackNormal( tex2D( _WaveNormal, panner17_g48 ) ) ) / 4.0 );
			#else
				float3 staticSwitch310 = half3(0,0,1);
			#endif
			float3 normalizeResult315 = normalize( staticSwitch310 );
			float temp_output_526_0 = ( _RefractionContribution * lerp((float)1,saturate( distanceDepth532 ),_ModulateRefractionbyDepth) * (normalizeResult315).x );
			float4 screenColor456 = tex2Dproj( _GrabTexture, UNITY_PROJ_COORD( ( ase_grabScreenPosNorm + temp_output_526_0 ) ) );
			float eyeDepth3_g98 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD( ( ase_grabScreenPosNorm + temp_output_526_0 ) ))));
			float4 lerpResult461 = lerp( ( _Shallow * screenColor456 ) , _Deep , saturate( ( ( eyeDepth3_g98 - i.eyeDepth ) / _DepthFade ) ));
			float temp_output_12_0_g101 = saturate( i.vertexToFrag11_g101 );
			float4 lerpResult465 = lerp( lerpResult461 , _Far , temp_output_12_0_g101);
			half3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult418 = dot( ase_worldViewDir , -ase_worldlightDir );
			float2 appendResult20_g95 = (half2(ase_worldPos.x , ase_worldPos.z));
			float temp_output_42_0 = ( UNITY_PI * _WaveDirection1 );
			float2 appendResult44 = (half2(cos( temp_output_42_0 ) , sin( temp_output_42_0 )));
			float2 temp_output_2_0_g95 = appendResult44;
			float dotResult3_g95 = dot( appendResult20_g95 , temp_output_2_0_g95 );
			float temp_output_7_0_g95 = _Wavelength1;
			float temp_output_8_0_g95 = ( ( _TimeValue * _Speed1 ) + ( ( dotResult3_g95 * UNITY_PI ) / temp_output_7_0_g95 ) );
			float temp_output_14_0_g95 = sin( temp_output_8_0_g95 );
			float temp_output_18_0_g95 = _Amplitude1;
			float3 appendResult126_g95 = (half3(0.0 , ( ( 1.0 - abs( temp_output_14_0_g95 ) ) * temp_output_18_0_g95 ) , 0.0));
			float2 appendResult20_g94 = (half2(ase_worldPos.x , ase_worldPos.z));
			float temp_output_115_0 = ( UNITY_PI * _WaveDirection2 );
			float2 appendResult117 = (half2(cos( temp_output_115_0 ) , sin( temp_output_115_0 )));
			float2 temp_output_2_0_g94 = appendResult117;
			float dotResult3_g94 = dot( appendResult20_g94 , temp_output_2_0_g94 );
			float temp_output_7_0_g94 = _Wavelength2;
			float temp_output_8_0_g94 = ( ( _TimeValue * _Speed2 ) + ( ( dotResult3_g94 * UNITY_PI ) / temp_output_7_0_g94 ) );
			float temp_output_14_0_g94 = sin( temp_output_8_0_g94 );
			float temp_output_18_0_g94 = _Amplitude2;
			float3 appendResult126_g94 = (half3(0.0 , ( ( 1.0 - abs( temp_output_14_0_g94 ) ) * temp_output_18_0_g94 ) , 0.0));
			half3 VertexOffset191 = ( appendResult126_g95 + appendResult126_g94 );
			half3 ase_worldNormal = WorldNormalVector( i, half3( 0, 0, 1 ) );
			half3 ase_worldTangent = WorldNormalVector( i, half3( 1, 0, 0 ) );
			half3 ase_worldBitangent = WorldNormalVector( i, half3( 0, 1, 0 ) );
			float3x3 ase_tangentToWorldFast = float3x3(ase_worldTangent.x,ase_worldBitangent.x,ase_worldNormal.x,ase_worldTangent.y,ase_worldBitangent.y,ase_worldNormal.y,ase_worldTangent.z,ase_worldBitangent.z,ase_worldNormal.z);
			float fresnelNdotV2_g97 = dot( mul(ase_tangentToWorldFast,normalizeResult315), ase_worldViewDir );
			float fresnelNode2_g97 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV2_g97, 5.0 ) );
			float temp_output_3_0_g97 = saturate( fresnelNode2_g97 );
			float fresnelNdotV2_g104 = dot( mul(ase_tangentToWorldFast,normalizeResult315), ase_worldViewDir );
			float fresnelNode2_g104 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV2_g104, 5.0 ) );
			float temp_output_3_0_g104 = saturate( fresnelNode2_g104 );
			float temp_output_12_0_g103 = saturate( i.vertexToFrag11_g103 );
			#ifdef _REFLECTIONENABLED_ON
				float4 staticSwitch412 = ( texCUBE( _ReflectionCubemap, WorldReflectionVector( i , normalizeResult315 ) ) * temp_output_3_0_g104 * ( 1.0 - temp_output_12_0_g103 ) );
			#else
				float4 staticSwitch412 = float4( 0,0,0,0 );
			#endif
			float mulTime6_g107 = _TimeValue * _FoamSpeed;
			float Time23_g107 = mulTime6_g107;
			float2 appendResult3_g102 = (half2(ase_worldPos.x , ase_worldPos.z));
			float2 Coordinates22_g107 = ( half3( ( appendResult3_g102 / _FoamScale ) ,  0.0 ) + ( _FoamNoiseScale * normalizeResult315 ) ).xy;
			float2 panner14_g107 = ( Time23_g107 * half2( 0.1,0.1 ) + Coordinates22_g107);
			float2 panner15_g107 = ( Time23_g107 * half2( -0.1,-0.1 ) + ( Coordinates22_g107 + half2( 0.418,0.355 ) ));
			float2 panner16_g107 = ( Time23_g107 * half2( -0.1,0.1 ) + ( Coordinates22_g107 + half2( 0.865,0.148 ) ));
			float2 panner17_g107 = ( Time23_g107 * half2( 0.1,-0.1 ) + ( Coordinates22_g107 + half2( 0.651,0.752 ) ));
			float temp_output_12_0_g105 = saturate( i.vertexToFrag11_g105 );
			#ifdef _FOAMENABLED_ON
				float4 staticSwitch439 = ( saturate( ( _FoamHeightMultiplier * ( _FoamHeightOffset + (VertexOffset191).y ) ) ) * ( ( tex2D( _Foam, panner14_g107 ) + tex2D( _Foam, panner15_g107 ) + tex2D( _Foam, panner16_g107 ) + tex2D( _Foam, panner17_g107 ) ) / 4.0 ) * ( 1.0 - temp_output_12_0_g105 ) );
			#else
				float4 staticSwitch439 = float4( 0,0,0,0 );
			#endif
			float mulTime22_g93 = _TimeValue * _SparkleSpeed;
			float2 appendResult3_g90 = (half2(ase_worldPos.x , ase_worldPos.z));
			float2 temp_output_594_0 = ( appendResult3_g90 / _SparkleScale );
			float2 temp_output_17_0_g93 = temp_output_594_0;
			float4 break16_g93 = float4( 1,2,3,4 );
			float2 panner6_g93 = ( mulTime22_g93 * float2( 0.1,0 ) + ( temp_output_17_0_g93 * break16_g93.x ));
			float2 panner7_g93 = ( mulTime22_g93 * float2( -0.1,0 ) + ( temp_output_17_0_g93 * break16_g93.y ));
			float3 appendResult24_g93 = (half3(UnpackNormal( tex2D( _SparkleNormalMap, panner6_g93 ) ).r , UnpackNormal( tex2D( _SparkleNormalMap, panner7_g93 ) ).g , 1.0));
			float2 panner5_g93 = ( mulTime22_g93 * float2( 0,0.1 ) + ( temp_output_17_0_g93 * break16_g93.z ));
			float2 panner8_g93 = ( mulTime22_g93 * float2( 0,-0.1 ) + ( temp_output_17_0_g93 * break16_g93.w ));
			float3 appendResult25_g93 = (half3(UnpackNormal( tex2D( _SparkleNormalMap, panner5_g93 ) ).r , UnpackNormal( tex2D( _SparkleNormalMap, panner8_g93 ) ).g , 1.0));
			float3 temp_output_571_0 = BlendNormals( appendResult24_g93 , appendResult25_g93 );
			float3 normalizeResult578 = normalize( temp_output_571_0 );
			float mulTime22_g92 = _TimeValue * _SparkleSpeed;
			float2 temp_output_17_0_g92 = temp_output_594_0;
			float4 break16_g92 = float4( 1,0.5,2.5,2 );
			float2 panner6_g92 = ( mulTime22_g92 * float2( 0.1,0 ) + ( temp_output_17_0_g92 * break16_g92.x ));
			float2 panner7_g92 = ( mulTime22_g92 * float2( -0.1,0 ) + ( temp_output_17_0_g92 * break16_g92.y ));
			float3 appendResult24_g92 = (half3(UnpackNormal( tex2D( _SparkleNormalMap, panner6_g92 ) ).r , UnpackNormal( tex2D( _SparkleNormalMap, panner7_g92 ) ).g , 1.0));
			float2 panner5_g92 = ( mulTime22_g92 * float2( 0,0.1 ) + ( temp_output_17_0_g92 * break16_g92.z ));
			float2 panner8_g92 = ( mulTime22_g92 * float2( 0,-0.1 ) + ( temp_output_17_0_g92 * break16_g92.w ));
			float3 appendResult25_g92 = (half3(UnpackNormal( tex2D( _SparkleNormalMap, panner5_g92 ) ).r , UnpackNormal( tex2D( _SparkleNormalMap, panner8_g92 ) ).g , 1.0));
			float3 temp_output_572_0 = BlendNormals( appendResult24_g92 , appendResult25_g92 );
			float3 normalizeResult580 = normalize( temp_output_572_0 );
			float dotResult582 = dot( normalizeResult578 , normalizeResult580 );
			float dotResult575 = dot( (temp_output_571_0).x , (temp_output_572_0).x );
			float temp_output_577_0 = sqrt( saturate( dotResult575 ) );
			#ifdef _SPARKLEENABLED_ON
				float staticSwitch615 = ceil( saturate( pow( ( dotResult582 * saturate( ( temp_output_577_0 + temp_output_577_0 + temp_output_577_0 ) ) ) , _SparkleExponent ) ) );
			#else
				float staticSwitch615 = 0.0;
			#endif
			float4 lerpResult637 = lerp( ( ( ( lerpResult465 + lerp(float4( 0,0,0,0 ),( _SSSColor * ( saturate( dotResult418 ) * saturate( (VertexOffset191).y ) * temp_output_3_0_g97 ) ),_SSSEnabled) ) + staticSwitch412 ) + staticSwitch439 ) , _SparkleColor , staticSwitch615);
			float dotResult623 = dot( ase_worldlightDir , normalize( WorldReflectionVector( i , normalizeResult315 ) ) );
			float4 lerpResult639 = lerp( lerpResult637 , _SunSpecularColor , lerp(0.0,round( saturate( pow( (0.0 + (dotResult623 - -1.0) * (1.0 - 0.0) / (1.0 - -1.0)) , _SunSpecularExponent ) ) ),_SunSpecularEnabled));
			float fresnelNdotV2_g99 = dot( mul(ase_tangentToWorldFast,normalizeResult315), ase_worldViewDir );
			float fresnelNode2_g99 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV2_g99, 5.0 ) );
			float temp_output_3_0_g99 = saturate( fresnelNode2_g99 );
			float screenDepth492 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD( ase_screenPos ))));
			float distanceDepth492 = abs( ( screenDepth492 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( ( _EdgeFoamDepth * (0.25 + (temp_output_3_0_g99 - 0.0) * (1.0 - 0.25) / (1.0 - 0.0)) ) ) );
			float2 appendResult3_g100 = (half2(ase_worldPos.x , ase_worldPos.z));
			float3 appendResult503 = (half3(( appendResult3_g100 / _EdgeFoamNoiseScale ) , _TimeValue));
			float simplePerlin3D505 = snoise( appendResult503 );
			#ifdef _EDGEFOAMENABLED_ON
				float staticSwitch510 = round( ( ( 1.0 - saturate( distanceDepth492 ) ) * (0.25 + (( 1.0 - simplePerlin3D505 ) - 0.0) * (1.0 - 0.25) / (1.0 - 0.0)) ) );
			#else
				float staticSwitch510 = 0.0;
			#endif
			float4 lerpResult641 = lerp( lerpResult639 , _EdgeFoamColor , staticSwitch510);
			o.Emission = lerpResult641.rgb;
			o.Alpha = _Alpha;
		}

		ENDCG
	}
	Fallback "Diffuse"
}