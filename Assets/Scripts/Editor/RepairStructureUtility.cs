﻿using UnityEditor;
using UnityEngine;

public class RepairStructureUtility : EditorWindow {

    private GameObject Object;
    private RepairStructure repairStructure;
    private int numOfChunks;

    [MenuItem("Tools/RepairStructure Utility")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        RepairStructureUtility window = (RepairStructureUtility)GetWindow(typeof(RepairStructureUtility));
        window.Show();
    }

    void OnGUI() {
        repairStructure = (RepairStructure)EditorGUILayout.ObjectField("Repair Structure : ", repairStructure, typeof(RepairStructure), true);
        Object = (GameObject)EditorGUILayout.ObjectField("Parent of Pieces : ", Object, typeof(GameObject), true);
        numOfChunks = EditorGUILayout.DelayedIntField("Number of Chunks", numOfChunks);

        if (GUILayout.Button("Create Chunks")) {
            Transform[] pieces = new Transform[Object.transform.childCount];
            int index = 0;
            foreach(Transform child in Object.transform) {
                pieces[index] = child;
                index++;
            }

            int numOfPiecesPerChunks = pieces.Length / numOfChunks;
            int numOfPiecesOfLastChunk = numOfPiecesPerChunks + pieces.Length % numOfChunks;
            int numOfPieces = 0;
            for (int c = 0; c < numOfChunks; c++) {
                RepairStructure.Chunk chunk = new RepairStructure.Chunk();
                if(c == numOfChunks - 1) {
                    numOfPieces = numOfPiecesOfLastChunk;
                } else {
                    numOfPieces = numOfPiecesPerChunks;
                }
                chunk.pieces = new Transform[numOfPieces];
                //chunk.oldPositions = new Vector3[numOfPiecesPerChunks];
                //chunk.oldRotations = new Quaternion[numOfPiecesPerChunks];
                for (int i = 0; i < numOfPieces; i++) {
                    chunk.pieces[i] = pieces[numOfPiecesPerChunks * c + i];
                    //chunk.oldPositions[i] = chunk.pieces[i].localPosition;
                    //chunk.oldRotations[i] = chunk.pieces[i].localRotation;
                }
                repairStructure.chunks.Add(chunk);
            }
        }
    }
}