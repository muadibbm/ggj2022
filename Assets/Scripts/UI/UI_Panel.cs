﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class UI_Panel : MonoBehaviour
{
    private Animator anim;
    private int animVisibleParam;
    private Coroutine hideRoutine;

    public bool IsVisible { get; private set; }

    protected virtual void Awake() {
        anim = GetComponent<Animator>();
        animVisibleParam = Animator.StringToHash("Visible");
    }

    public virtual void Show() {
        if (IsVisible == true) return;
        IsVisible = true;
        gameObject.SetActive(true);
        anim.SetBool(animVisibleParam, true);
        if (hideRoutine != null) {
            StopCoroutine(hideRoutine);
            hideRoutine = null;
        }
    }

    public virtual void Hide() {
        if (IsVisible == false) return;
        IsVisible = false;
        anim.SetBool(animVisibleParam, false);
        //anim.ResetTrigger(animVariationParam);
        if (hideRoutine != null) StopCoroutine(hideRoutine);
        hideRoutine = StartCoroutine(DelayedHide(1f)); // the animator hide has to take exactly this amount of seconds
    }

    private IEnumerator DelayedHide(float delay) {
        yield return new WaitForSeconds(delay);
        gameObject.SetActive(false);
        hideRoutine = null;
    }
}