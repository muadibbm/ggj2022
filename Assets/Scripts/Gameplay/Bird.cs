using UnityEngine;

public class Bird : Interact
{
    [SerializeField] Dialogue dialogue;
    [SerializeField] Enum.InstrumentType[] instruments;
    [SerializeField] float melodyDuration;
    [SerializeField] Renderer rend;

    [Header("State Controller")]
    [SerializeField] ObjectStateController stateController;

    [SerializeField] bool isSimorgh;

    private float[] melodyValues;
    private float[] finalMelodyValues;
    private bool interacting;
    private bool usingInstrument;
    private cakeslice.Outline outline;
    private Collider col;
    private Animator anim;

    private void Awake() {
        anim = GetComponent<Animator>();
        if(isSimorgh == false) anim.Play("Idle", -1, Random.Range(0f, 0.9f));
        melodyValues = new float[instruments.Length];
        finalMelodyValues = new float[instruments.Length];
        outline = rend.gameObject.AddComponent<cakeslice.Outline>();
        col = GetComponent<Collider>();
    }

    public void SetNewDialogue(Dialogue dialogue, ObjectStateController stateController = null) {
        this.dialogue = dialogue;
        this.stateController = stateController;
        for(int i = 0; i < melodyValues.Length; i++) {
            melodyValues[i] = 0f;
        }
        enabled = true;
        col.enabled = true;
    }

    private void Start() {
        outline.enabled = false;
    }

    public override void BeginInteract() {
        interacting = true;
    }

    public override void EndInteract() {
        interacting = false;
        usingInstrument = false;
        RemoveOutline();
    }

    public override void PauseInteract() {
        usingInstrument = false;
    }

    public override void UpdateInteract() {
        usingInstrument = true;
    }

    public override float GetMelodyDuration() {
        return melodyDuration;
    }

    public override float[] GetCurrentMelodyValues() {
        return finalMelodyValues;
    }

    public override Enum.InstrumentType[] GetRequiredInstruments() {
        return instruments;
    }

    public override float GetInstrumentValue(Enum.InstrumentType instrument) {
        for (int i = 0; i < instruments.Length; i++) {
            if (instruments[i] == instrument) {
                return melodyValues[i];
            }
        }
        return -1;
    }

    private void Update() {
        Enum.InstrumentType instrument = GameManager.Instance.GetPlayerController().CurrentInstrument;
        bool allInstrumentsPlayed = true;
        for (int i = 0; i < instruments.Length; i++) {
            if (melodyValues[i] < melodyDuration) {
                if (interacting) {
                    if (instruments[i] == instrument) {
                        if (usingInstrument) {
                            melodyValues[i] = Mathf.Clamp(melodyValues[i] + Time.deltaTime, 0f, melodyDuration);
                        } else {
                            if (melodyValues[i] > 0) melodyValues[i] = Mathf.Clamp(melodyValues[i] - Time.deltaTime * 6, 0f, melodyDuration);
                        }
                        AddOutline(0);
                    } else {
                        if(isSimorgh) AddOutline(0); 
                        else AddOutline(1);
                        if (melodyValues[i] > 0) melodyValues[i] = Mathf.Clamp(melodyValues[i] - Time.deltaTime * 6, 0f, melodyDuration);
                    }
                } else {
                    if (melodyValues[i] > 0) melodyValues[i] = Mathf.Clamp(melodyValues[i] - Time.deltaTime * 6, 0f, melodyDuration);
                }
                allInstrumentsPlayed = false;
            } else {
                RemoveOutline();
            }
            finalMelodyValues[i] = melodyValues[i] / melodyDuration;
        }
        if (allInstrumentsPlayed) {
            if (isSimorgh == false) anim.SetBool("Talk", true);
            if(isSimorgh) GameManager.Instance.GetPlayerController().PlayCompleteInstrumentAudio(instruments, melodyDuration);
            GameManager.Instance.GetPlayerController().StartDialogue(dialogue, ActivateStateController);
            enabled = false;
            col.enabled = false;
            RemoveOutline();
        }
    }

    private void ActivateStateController() {
        if (stateController != null) {
            stateController.Activate();
        }
    }

    private void AddOutline(int outlineIndex) {
        outline.enabled = true;
        outline.color = outlineIndex;
    }

    private void RemoveOutline() {
        outline.enabled = false;
    }
}
