﻿using UnityEngine;

public class WaveController : MonoBehaviour
{
	[SerializeField] Renderer[] renderers;
	[SerializeField] Vector3 waveSource = new Vector3(3000f, 0f, 3000f);
	[SerializeField] float wave1Speed = 0.1f;
	[SerializeField] float wave1Length = 50f;
	[SerializeField] float wave1Amplitude = 1f;

	private int _waveSourceX;
	private int _waveSourceZ;
	private int _speed1;
	private int _wavelength1;
	private int _amplitude1;
	private int _time;
	private int _sparkleSpeed;

	private Material [] materials;

	private void Awake() {
		materials = new Material[renderers.Length];
		for (int i = 0; i < renderers.Length; i++)
			materials[i] = renderers[i].material;
		_time = Shader.PropertyToID("_TimeValue");
		_waveSourceX = Shader.PropertyToID("_WaveSourceX");
		_waveSourceZ = Shader.PropertyToID("_WaveSourceZ");
		_speed1 = Shader.PropertyToID("_Speed1");
		_wavelength1 = Shader.PropertyToID("_Wavelength1");
		_amplitude1 = Shader.PropertyToID("_Amplitude1");
		_sparkleSpeed = Shader.PropertyToID("_SparkleSpeed");
	}

	private void Update() {
		for (int i = 0; i < materials.Length; i++)
			UpdateMaterial(materials[i]);
	}

    private void UpdateMaterial(Material material) {
		material.SetFloat(_time, Time.time * Time.timeScale);
		material.SetFloat(_speed1, wave1Speed);
		material.SetFloat(_wavelength1, wave1Length);
		material.SetFloat(_amplitude1, wave1Amplitude);
		material.SetFloat(_sparkleSpeed, wave1Amplitude * 0.1f);
		material.SetFloat(_waveSourceX, waveSource.x);
		material.SetFloat(_waveSourceZ, waveSource.z);
	}

	private Vector3 ApplyWave(Vector3 point) {
		point.y = 0.0f;
		float dist = Vector3.Distance(point, waveSource);
		dist = (dist % wave1Length) / wave1Length;
		float y1 = wave1Amplitude * Mathf.Sin(2f * Mathf.PI * (Time.time * wave1Speed + dist));
		//dist = (dist % wc.wave2Length) / wc.wave2Length;
		//float y2 = wc.wave2Amplitude * Mathf.Sin(Mathf.PI * (Time.time * wc.wave2Speed + dist));
		point.y = y1; // + y2
		return point;
	}
}

