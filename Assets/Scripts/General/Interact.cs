﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public abstract class Interact : MonoBehaviour
{
    [SerializeField] float interactionDistance = 10f;
    public float InteractionDistance { get { return interactionDistance; } }

    public abstract void UpdateInteract();
    public abstract void PauseInteract();
    public abstract void BeginInteract();
    public abstract void EndInteract();

    [SerializeField] Color debugColor = Color.green;

    private SphereCollider[] colliders;

    private void OnDrawGizmosSelected() {
        colliders = GetComponents<SphereCollider>();
        if (colliders != null && colliders.Length != 0) {
            Gizmos.color = debugColor;
            for (int i = 0; i < colliders.Length; i++) {
                Gizmos.DrawWireSphere(transform.position + transform.rotation * colliders[i].center, colliders[i].radius);
                Gizmos.DrawLine(transform.position, transform.position + transform.rotation * colliders[i].center);
            }
        }
    }

    // Gameplay

    public abstract Enum.InstrumentType [] GetRequiredInstruments();
    public abstract float [] GetCurrentMelodyValues();
    public abstract float GetInstrumentValue(Enum.InstrumentType instrument);
    public abstract float GetMelodyDuration();
}
