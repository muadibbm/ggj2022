using UnityEngine;
using UnityEngine.UI;

public class UI_RequiredInstruments : UI_Panel
{
    [SerializeField] Transform canvasTransform;
    [SerializeField] Sprite[] instruments;
    [SerializeField] Image[] requiredInstrumentsParent;
    [SerializeField] Image[] requiredInstrumentsIcon;
    [SerializeField] Image[] requiredInstrumentFill;

    Vector3 playerPos;

    private void Update() {
        playerPos = GameManager.Instance.GetPlayer().transform.position;
        playerPos.y = canvasTransform.position.y;
        canvasTransform.LookAt(playerPos, Vector3.up);
    }

    public void Show(Vector3 position, Enum.InstrumentType[] requiredInstruments, float[] melodyValues) {
        Show();
        canvasTransform.position = position;
        for (int i = requiredInstrumentsIcon.Length - 1; i >= requiredInstruments.Length; i--) {
            requiredInstrumentsParent[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < requiredInstruments.Length; i++) {
            requiredInstrumentsIcon[i].sprite = instruments[(int)requiredInstruments[i]];
            requiredInstrumentFill[i].fillAmount = melodyValues[i];
            requiredInstrumentsParent[i].gameObject.SetActive(true);
        }
    }
}
