﻿using System.Collections;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public void Fade(AudioSource audioSource, float targetVolume, float duration) {
        StartCoroutine(FadeRoutine(audioSource, targetVolume, duration));
    }

    public void Transition(AudioSource audioSourceFrom, AudioSource audioSourceTo, AudioSource transition, float duration = 3f) {
        StartCoroutine(FadeRoutine(audioSourceFrom, 0, duration));
        StartCoroutine(FadeRoutine(transition, 1, 3, duration / 2f));
        StartCoroutine(FadeRoutine(audioSourceTo, 1, duration, duration));
    }

    private IEnumerator FadeRoutine(AudioSource audioSource, float targetVolume, float duration, float delay = 0f) {
        yield return new WaitForSeconds(delay);
        float startVolume = audioSource.volume;
        float time = 0f;
        if (audioSource.isPlaying == false) audioSource.Play();
        while (time <= duration) {
            audioSource.volume = Mathf.Lerp(startVolume, targetVolume, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        if(audioSource.volume <= 0) {
            audioSource.Stop();
        }
    }
}
